# -*- conf -*-
# {{ansible_managed}}

domain-needed
bogus-priv
no-resolv
no-poll
no-hosts
expand-hosts

dhcp-authoritative
dhcp-rapid-commit

{% if dnsmasq_log_queries %}
log-queries
{% endif %}

{% if dnsmasq_log_dhcp %}
log-dhcp
{% endif %}

{% for item in dnsmasq_dns_servers %}
server={{item}}
{% endfor %}

local=/{{dnsmasq_domain_name}}/
domain={{dnsmasq_domain_name}}

{% if dnsmasq_allow_firefox_dns_over_http %}
address=/use-application-dns.net/
{% endif %}

{% for item in dnsmasq_hosts %}
{% if 'ip' in item %}
host-record={{item.name}}, {{item.name}}.{{dnsmasq_domain_name}}, {{item.ip}}
{% endif %}
{% endfor %}

{%- for item in dnsmasq_cnames %}
cname={{item.name}}, {{item.name}}.{{dnsmasq_domain_name}}, {{item.target}}
{% endfor %}

{% for item in dnsmasq_dhcp_networks %}
{%- set start_ip = item['start_ip'] | default(item.network | ansible.netcommon.nthhost(100)) %}
{% set end_ip = item['end_ip'] | default(item.network | ansible.netcommon.nthhost(-6)) %}
{% set netmask = item['netmask'] | default(item.network | ansible.netcommon.ipaddr('netmask')) %}
{% set router = item['router'] | default(item.network | ansible.netcommon.nthhost(1)) %}
{% set dns_server = item['dns_server'] | default(item.network | ansible.netcommon.nthhost(2)) -%}

dhcp-range=set:{{item.name}}, {{start_ip}}, {{end_ip}}, {{netmask}}, {{dnsmasq_dhcp_lease_time}}
dhcp-option=tag:{{item.name}}, option:router, {{router}}
dhcp-option=tag:{{item.name}}, option:dns-server, {{dns_server}}

{% endfor %}

{% for item in dnsmasq_hosts %}
{% if not 'static' in item or not item.static %}
dhcp-host=
  {%- if 'mac' in item %}{{item.mac}}, {% endif %}
  {%- if 'ip' in item %}{{item.ip}}, {% endif -%}
  {{item.name}}
{% endif %}
{% endfor %}

# If a DHCP client claims that its name is "wpad", ignore that.
# This fixes a security hole. See CERT Vulnerability VU#598349
dhcp-name-match=set:wpad-ignore,wpad
dhcp-ignore-names=tag:wpad-ignore

{% for item in dnsmasq_additional_settings %}
{{item}}
{% endfor %}
